package com.puravida.gradle.gitlab

import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskExecutionException

class InstallHookTask extends DefaultTask{

    @Optional
    final Property<Boolean> overwrite =  project.objects.property(Boolean)

    @TaskAction
    void main(){

        File gitDir = project.file('.git/hooks')
        if( gitDir.exists() == false ){
            throw new TaskExecutionException(this,new RuntimeException('There isn\'t a git repository'))
        }

        File prepush = new File(gitDir, "pre-push")
        if( prepush.exists() ){
            if( overwrite.get() == false)
                throw new TaskExecutionException(this,new RuntimeException('There isn\'t a git repository'))
            else
                logger.info("Overwritting pre-push hook")
        }

        prepush.text = '''#!/bin/sh
./gradlew validateGitlabCI
'''
        prepush.executable = true
    }

}
