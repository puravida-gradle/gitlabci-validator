package com.puravida.gradle.gitlab
import org.gradle.api.Project
import org.gradle.api.provider.Property
class GitlabExtension {

    final Property<String> api

    final Property<String> oveerwriteHook

    GitlabExtension(Project project) {
        api = project.objects.property(String)
        api.set('v4')

        oveerwriteHook = project.objects.property(Boolean)
        oveerwriteHook.set(true)
    }

    GitlabExtension api(String v){
        api.set(v)
    }

    GitlabExtension oveerwriteHook(boolean b){
        oveerwriteHook.set(b)
    }

}
