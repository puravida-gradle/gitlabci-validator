package com.puravida.gradle.gitlab

import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.DependencySet

class GitlabCIPlugin implements Plugin<Project> {

    void apply(Project project) {
        project.with {
            apply(plugin: 'base')

            GitlabExtension gitlabExtension = extensions.create('gitlabCI', GitlabExtension, project)

            tasks.create('installValidateGitlabCIHook',InstallHookTask, new Action<InstallHookTask>() {
                @Override
                void execute(InstallHookTask t) {
                    t.overwrite.set( gitlabExtension.oveerwriteHook )
                }
            }).group = 'verification'

            tasks.create('validateGitlabCI',GitlabCIValidatorTask, new Action<GitlabCIValidatorTask>() {
                @Override
                void execute(GitlabCIValidatorTask t) {
                    t.api.set( gitlabExtension.api )
                }
            }).group = 'verification'

        }
    }
}
