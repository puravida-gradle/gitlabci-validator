package com.puravida.gradle.gitlab

import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.StopExecutionException
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskExecutionException
import org.gradle.tooling.BuildException
import org.yaml.snakeyaml.Yaml

import static groovy.json.JsonOutput.prettyPrint
import static groovy.json.JsonOutput.toJson
import static groovyx.net.http.ContentTypes.JSON
import static groovyx.net.http.HttpBuilder.configure

class GitlabCIValidatorTask extends DefaultTask{

    @Input
    final Property<String> api =  project.objects.property(String)

    @Internal
    String getUrl(){
        "https://gitlab.com/api/${api.get()}/ci/lint"
    }

    @TaskAction
    void main(){

        File gitlabci = project.file('.gitlab-ci.yml')
        if( gitlabci.exists() == false)
            return

        def yaml = new Yaml().load(gitlabci.newInputStream())

        def resp = configure {
            request.uri = url
            request.contentType = JSON[0]
        }.post{
            request.body = [
                    content: toJson(yaml)
            ]
        }
        if( resp.status != "valid")
            throw new TaskExecutionException(this,new RuntimeException("$resp.errors".toString()))
    }

}
