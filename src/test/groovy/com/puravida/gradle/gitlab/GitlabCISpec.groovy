package com.puravida.gradle.gitlab

import org.gradle.api.tasks.TaskExecutionException
import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

import static org.gradle.testkit.runner.TaskOutcome.FAILED
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class GitlabCISpec extends Specification{

    @Rule
    TemporaryFolder testProjectDir

    File buildFile

    def setup() {
        buildFile = testProjectDir.newFile('build.gradle')
        buildFile << """
// tag::addPlugin[]

            plugins {
                id 'com.puravida.gradle.gitlabci-validator'
            }
                        
// end::addPlugin[]
            """
    }

    def "run task"() {
        testProjectDir.newFile('.gitlab-ci.yml').text = '''
build-gradle:
 image: gradle
 script:
 - ./gradlew build
 stage: build
 artifacts:
  paths:
   - build/asciidoc

pages:
  stage: deploy
  dependencies:
    - build-gradle
  script:
    - mkdir -p public
    - cp -R build/asciidoc/html5/* public/
  artifacts:
    paths:
    - public
  when: manual
'''
        buildFile << """

// tag::executeTask[]
            validateGitlabCI{                          
            }
// end::executeTask[]
            
        """

        when:

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                'validateGitlabCI',
                '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":validateGitlabCI").outcome == SUCCESS
    }

    def "run invalid gitlabci task"() {
        testProjectDir.newFile('.gitlab-ci.yml').text = '''
 build-gradle:
 image: gradle
'''
        buildFile << """

// tag::executeInvalidTask[]
            validateGitlabCI{                          
            }
// end::executeInvalidTask[]
            
        """

        when:

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                        'validateGitlabCI',
                        '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        thrown Exception
    }

    def "set api version task"() {
        testProjectDir.newFile('.gitlab-ci.yml').text = '''
build-gradle:
 image: gradle
 script:
 - ./gradlew build
 stage: build
 artifacts:
  paths:
   - build/asciidoc

pages:
  stage: deploy
  dependencies:
    - build-gradle
  script:
    - mkdir -p public
    - cp -R build/asciidoc/html5/* public/
  artifacts:
    paths:
    - public
  when: manual
'''
        buildFile << """

// tag::executeApiTask[]
            gitlabCI{
                api 'v4'
            }
            validateGitlabCI{                          
            }
// end::executeApiTask[]
            
        """

        when:

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                        'validateGitlabCI',
                        '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":validateGitlabCI").outcome == SUCCESS
    }

}
