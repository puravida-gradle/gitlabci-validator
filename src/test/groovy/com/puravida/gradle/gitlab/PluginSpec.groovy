package com.puravida.gradle.gitlab


import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class PluginSpec  extends Specification{

    Project project

    def setup() {
        project = ProjectBuilder.builder().build()
    }

    @SuppressWarnings('MethodName')
    def "Applies plugin and checks default setup"() {

        expect:
        project.tasks.findByName("gitlabci") == null

        when:
        project.apply plugin: 'com.puravida.gradle.gitlabci-validator'

        then:
        Task task = project.tasks.findByName('validateGitlabCI')
        task != null

        project.tasks.findByName('clean') != null
    }

}
