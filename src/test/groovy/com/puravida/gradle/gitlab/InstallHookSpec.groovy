package com.puravida.gradle.gitlab
import org.gradle.api.tasks.TaskExecutionException
import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

import static org.gradle.testkit.runner.TaskOutcome.FAILED
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class InstallHookSpec extends Specification{

    @Rule
    TemporaryFolder testProjectDir

    File buildFile

    def setup() {
        buildFile = testProjectDir.newFile('build.gradle')
        buildFile << """
// tag::addPlugin[]

            plugins {
                id 'com.puravida.gradle.gitlabci-validator'
            }
                        
// end::addPlugin[]
            """
    }

    def "run task"() {
        testProjectDir.newFolder('.git','hooks')

        when:

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                        'installValidateGitlabCIHook',
                        '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":installValidateGitlabCIHook").outcome == SUCCESS

        and:
        new File(testProjectDir.root, '.git/hooks/pre-push').exists()

        and:
        new File(testProjectDir.root, '.git/hooks/pre-push').text == '''#!/bin/sh
./gradlew validateGitlabCI
'''

    }
}
